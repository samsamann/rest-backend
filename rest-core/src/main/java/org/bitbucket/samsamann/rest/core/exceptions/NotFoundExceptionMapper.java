package org.bitbucket.samsamann.rest.core.exceptions;

import io.crnk.core.engine.document.ErrorData;
import io.crnk.core.engine.document.ErrorDataBuilder;
import io.crnk.core.engine.error.ErrorResponse;

/**
 * Is a abstract exception mapper for not found (404) exceptions
 */
abstract class NotFoundExceptionMapper {

    private final static String TITLE_STRING = "Resource not found";

    private final static int HTTP_CODE = 404;

    ErrorResponse buildErrorResponse(String detail) {
        ErrorDataBuilder builder = ErrorData.builder();
        builder = builder.setStatus(String.valueOf(HTTP_CODE));
        builder = builder.setDetail(detail);
        builder = builder.setTitle(TITLE_STRING);
        return ErrorResponse.builder().setStatus(HTTP_CODE).setSingleErrorData(builder.build()).build();
    }

    protected boolean accepts(ErrorResponse errorResponse) {
        return errorResponse.getHttpStatus() == HTTP_CODE;
    }
}
