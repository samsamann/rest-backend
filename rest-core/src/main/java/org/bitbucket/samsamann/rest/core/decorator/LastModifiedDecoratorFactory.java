package org.bitbucket.samsamann.rest.core.decorator;

import io.crnk.core.engine.http.HttpRequestContextProvider;
import io.crnk.core.repository.ResourceRepositoryV2;
import io.crnk.core.repository.decorate.RepositoryDecoratorFactoryBase;
import io.crnk.core.repository.decorate.ResourceRepositoryDecorator;
import org.bitbucket.samsamann.rest.base.entities.BaseEntity;

import java.io.Serializable;

/**
 * simple decorator factory for the last modified repository decorator
 */
public class LastModifiedDecoratorFactory extends RepositoryDecoratorFactoryBase {

    private HttpRequestContextProvider httpRequestContextProvider;

    public LastModifiedDecoratorFactory(HttpRequestContextProvider httpRequestContextProvider) {
        this.httpRequestContextProvider = httpRequestContextProvider;
    }

    @Override
    public <T, I extends Serializable> ResourceRepositoryDecorator<T, I> decorateRepository(ResourceRepositoryV2<T, I> repository) {
        if(BaseEntity.class.isAssignableFrom(repository.getResourceClass())) {
            LastModifiedRepoDecorator<T, I> lastModifiedDecorator =
                    new LastModifiedRepoDecorator<>(httpRequestContextProvider);
            lastModifiedDecorator.setDecoratedObject(repository);
            return lastModifiedDecorator;
        }
        return null;
    }
}
