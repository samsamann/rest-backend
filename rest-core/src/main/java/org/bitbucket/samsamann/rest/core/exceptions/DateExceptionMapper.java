package org.bitbucket.samsamann.rest.core.exceptions;

import io.crnk.core.engine.document.ErrorData;
import io.crnk.core.engine.document.ErrorDataBuilder;
import io.crnk.core.engine.error.ErrorResponse;
import io.crnk.core.engine.error.ExceptionMapper;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.time.DateTimeException;

/**
 * exception mapper when something goes wrong with a date calculation
 */
public class DateExceptionMapper implements ExceptionMapper<DateTimeException> {

    private static final int HTTP_CODE = 400;

    private static final String TITLE_STRING = "Date exception";

    @Override
    public ErrorResponse toErrorResponse(DateTimeException e) {
        ErrorDataBuilder builder = ErrorData.builder();
        builder = builder.setStatus(String.valueOf(HTTP_CODE));
        builder = builder.setDetail(e.getLocalizedMessage());
        builder = builder.setTitle(TITLE_STRING);
        return ErrorResponse.builder().setStatus(HTTP_CODE).setSingleErrorData(builder.build()).build();
    }

    @Override
    public DateTimeException fromErrorResponse(ErrorResponse errorResponse) {
        throw new NotImplementedException();
    }

    @Override
    public boolean accepts(ErrorResponse errorResponse) {
        return errorResponse.getHttpStatus() == HTTP_CODE;
    }
}
