package org.bitbucket.samsamann.rest.core.decorator;

import io.crnk.core.engine.http.HttpRequestContextProvider;
import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.decorate.ResourceRepositoryDecoratorBase;
import io.crnk.core.resource.list.ResourceList;
import org.bitbucket.samsamann.rest.base.entities.BaseEntity;

import javax.ws.rs.core.HttpHeaders;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Optional;

/**
 * determine and set last modified header
 */
public class LastModifiedRepoDecorator<T, I extends Serializable> extends ResourceRepositoryDecoratorBase<T, I> {

    private HttpRequestContextProvider httpRequestContextProvider;

    LastModifiedRepoDecorator(HttpRequestContextProvider httpRequestContextProvider) {
        this.httpRequestContextProvider = httpRequestContextProvider;
    }

    @Override
    public T findOne(I id, QuerySpec querySpec) {
        T result =  super.findOne(id, querySpec);
        setLastModifiedHeader(((BaseEntity)result).getUpdateDate());
        return result;
    }

    @Override
    public ResourceList<T> findAll(Iterable<I> ids, QuerySpec querySpec) {
        ResourceList<T> resourceList =  super.findAll(ids, querySpec);
        determineLastModifiedHeader(resourceList);
        return resourceList;
    }

    @Override
    public ResourceList<T> findAll(QuerySpec querySpec) {
        ResourceList<T> resourceList = super.findAll(querySpec);
        determineLastModifiedHeader(resourceList);
        return resourceList;
    }

    private void determineLastModifiedHeader(ResourceList<T> resourceList) {
        Optional<BaseEntity> optional = resourceList.stream()
                .filter(BaseEntity.class::isInstance)
                .map(o -> (BaseEntity) o)
                .max(Comparator.comparing(BaseEntity::getUpdateDate, LocalDateTime::compareTo));

        optional.ifPresent(obj -> setLastModifiedHeader(obj.getUpdateDate()));
    }

    private void setLastModifiedHeader(LocalDateTime localDateTime) {
        // @TODO: Change hard coded zone...
        ZoneId zoneId = ZoneId.of("Europe/Zurich");
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, zoneId);

        httpRequestContextProvider.getRequestContext().setResponseHeader(
                HttpHeaders.LAST_MODIFIED,
                zonedDateTime.format(DateTimeFormatter.RFC_1123_DATE_TIME)
        );
    }
}
