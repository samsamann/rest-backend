package org.bitbucket.samsamann.rest.core;

import io.crnk.core.boot.CrnkBoot;
import io.crnk.core.module.Module;
import org.bitbucket.samsamann.rest.core.decorator.LastModifiedDecoratorFactory;
import org.bitbucket.samsamann.rest.core.exceptions.DateExceptionMapper;
import org.bitbucket.samsamann.rest.core.exceptions.NoDataExceptionMapper;
import org.bitbucket.samsamann.rest.core.exceptions.RepositoryNotFoundExceptionMapper;
import org.bitbucket.samsamann.rest.core.exceptions.RestEasyNotFoundExceptionMapper;

/**
 * rest-core module add more exception mappers and is a wrapper for the jpa module
 */
class CoreModule implements Module {

    private final static String MODULE_NAME = "rest-core";

    private CrnkBoot boot;

    CoreModule(CrnkBoot boot) {
        this.boot = boot;

    }

    @Override
    public String getModuleName() {
        return MODULE_NAME;
    }

    @Override
    public void setupModule(ModuleContext moduleContext) {
        moduleContext.addExceptionMapper(new RestEasyNotFoundExceptionMapper());
        moduleContext.addExceptionMapper(new RepositoryNotFoundExceptionMapper());
        moduleContext.addExceptionMapper(new NoDataExceptionMapper());
        moduleContext.addExceptionMapper(new DateExceptionMapper());

        moduleContext.addRepositoryDecoratorFactory(
                new LastModifiedDecoratorFactory(boot.getModuleRegistry().getHttpRequestContextProvider())
        );
    }
}
