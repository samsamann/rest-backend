package org.bitbucket.samsamann.rest.core;

import io.crnk.core.engine.transaction.TransactionRunner;
import io.crnk.rs.CrnkFeature;
import io.crnk.rs.JsonApiResponseFilter;
import io.crnk.rs.JsonapiExceptionMapperBridge;
import io.crnk.validation.ValidationModule;
import org.bitbucket.samsamann.rest.base.config.RestBackendProperties;
import org.bitbucket.samsamann.rest.core.config.JpaModuleConfigurator;

import javax.persistence.EntityManager;
import javax.ws.rs.core.FeatureContext;

/**
 * Extend the existing crnk feature. configure additional filters and prepare the core/jpa module
 */
public class RestFeature extends CrnkFeature {

    private EntityManager entityManager;

    private TransactionRunner runner;

    public RestFeature(EntityManager entityManager, TransactionRunner runner) {
        this.entityManager = entityManager;
        this.runner = runner;
    }

    @Override
    public boolean configure(FeatureContext context) {
        setDefaultPageLimit(Long.decode(System.getProperty(RestBackendProperties.PAGE_LIMIT_PROP_NAME,"20L")));
        getBoot().setMaxPageLimit(RestBackendProperties.MAX_PAGE_LIMIT);

        addModules();
        registerAdditionalFeatures(context);

        return super.configure(context);
    }

    private void addModules() {
        addModule(new CoreModule(this.getBoot()));
        addModule(JpaModuleConfigurator.createJpaModule(entityManager, runner));
        addModule(ValidationModule.create());
    }

    private void registerAdditionalFeatures(FeatureContext context) {
        context.register(new JsonApiResponseFilter(this));
        context.register(new JsonapiExceptionMapperBridge(this));
    }
}
