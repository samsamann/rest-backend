package org.bitbucket.samsamann.rest.core.config;

import io.crnk.core.engine.transaction.TransactionRunner;
import io.crnk.jpa.JpaModule;
import io.crnk.jpa.JpaRepositoryConfig;
import io.crnk.jpa.annotations.JpaResource;
import org.bitbucket.samsamann.rest.core.discovery.JpaEntityDiscovery;

import javax.persistence.EntityManager;
import java.util.Set;

/**
 * Created by samuel on 14.09.17.
 */
public class JpaModuleConfigurator {

    public static JpaModule createJpaModule(EntityManager entityManager, TransactionRunner runner) {
        JpaModule module = JpaModule.newServerModule(entityManager, runner);
        Set<Class<?>> classes = JpaEntityDiscovery.getInstance().getEntityClassesWithAnnotation(JpaResource.class);
        classes.forEach(aClass -> {
            module.addRepository(JpaRepositoryConfig.create(aClass));
        });

        return module;
    }
}
