package org.bitbucket.samsamann.rest.core.exceptions;

import io.crnk.core.engine.error.ErrorResponse;
import io.crnk.core.engine.error.ExceptionMapper;
import io.crnk.core.exception.RepositoryNotFoundException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Capture and map RepositoryNotFoundExceptions
 */
public class RepositoryNotFoundExceptionMapper extends NotFoundExceptionMapper
        implements ExceptionMapper<RepositoryNotFoundException> {

    private static final String DETAIL_MESSAGE = "Wrong or type is missing (type attribute is required)";

    @Override
    public ErrorResponse toErrorResponse(RepositoryNotFoundException e) {
        return buildErrorResponse(DETAIL_MESSAGE);
    }

    @Override
    public RepositoryNotFoundException fromErrorResponse(ErrorResponse errorResponse) {
        throw new NotImplementedException();
    }

    @Override
    public boolean accepts(ErrorResponse errorResponse) {
        return super.accepts(errorResponse);
    }
}
