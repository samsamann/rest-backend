package org.bitbucket.samsamann.rest.core.exceptions;

import io.crnk.core.engine.error.ErrorResponse;
import io.crnk.core.engine.error.ExceptionMapper;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.ws.rs.NotFoundException;

/**
 * Capture an map RestEasyNotFoundException
 */
public class RestEasyNotFoundExceptionMapper extends NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {

    @Override
    public ErrorResponse toErrorResponse(NotFoundException e) {
        String message = e.getLocalizedMessage();
        return buildErrorResponse(message.substring(message.indexOf(":") + 1).trim());
    }

    @Override
    public NotFoundException fromErrorResponse(ErrorResponse errorResponse) {
        throw new NotImplementedException();
    }

    @Override
    public boolean accepts(ErrorResponse errorResponse) {
        return super.accepts(errorResponse);
    }
}
