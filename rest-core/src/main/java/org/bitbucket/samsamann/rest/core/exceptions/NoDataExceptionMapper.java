package org.bitbucket.samsamann.rest.core.exceptions;

import io.crnk.core.engine.document.ErrorData;
import io.crnk.core.engine.document.ErrorDataBuilder;
import io.crnk.core.engine.error.ErrorResponse;
import io.crnk.core.engine.error.ExceptionMapper;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.NoSuchElementException;

// @Todo: Inject request object for more information

/**
 * Simple no data exception mapper
 */
public class NoDataExceptionMapper implements ExceptionMapper<NoSuchElementException> {

    private static final int HTTP_CODE = 400;

    private static final String DETAIL_MESSAGE = "Request body not in the correct format";

    @Override
    public ErrorResponse toErrorResponse(NoSuchElementException e) {
        ErrorDataBuilder builder = ErrorData.builder();
        builder = builder.setStatus(String.valueOf(HTTP_CODE));
        builder = builder.setDetail(DETAIL_MESSAGE);
        builder = builder.setTitle(e.getMessage());
        return ErrorResponse.builder().setStatus(HTTP_CODE).setSingleErrorData(builder.build()).build();
    }

    @Override
    public NoSuchElementException fromErrorResponse(ErrorResponse errorResponse) {
        throw new NotImplementedException();
    }

    @Override
    public boolean accepts(ErrorResponse errorResponse) {
        return errorResponse.getHttpStatus() == HTTP_CODE;
    }
}
