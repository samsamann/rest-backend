package org.bitbucket.samsamann.rest.core.discovery;

import org.bitbucket.samsamann.rest.base.config.RestBackendProperties;
import org.reflections.Reflections;

import javax.persistence.Entity;
import java.lang.annotation.Annotation;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by samuel on 12.09.17.
 */
public class JpaEntityDiscovery {

    private static final String PACKAGE_REGEX_SPLITTER = ",";

    private static JpaEntityDiscovery instance;

    private Reflections reflections;

    private JpaEntityDiscovery() {
        String packageNames = System.getProperty(RestBackendProperties.PACKAGE_SCAN_PROP_NAME);
        reflections =  new Reflections(packageNames.trim().split(PACKAGE_REGEX_SPLITTER));
    }

    public static JpaEntityDiscovery getInstance() {
        if (instance == null) {
            instance = new JpaEntityDiscovery();
        }

        return instance;
    }

    public Set<Class<?>> getEntityClassesWithAnnotation(Class<? extends Annotation> annotation) {
        return reflections.getTypesAnnotatedWith(annotation)
                .stream()
                .filter(aClass -> aClass.isAnnotationPresent(Entity.class))
                .collect(Collectors.toSet());
    }
}
