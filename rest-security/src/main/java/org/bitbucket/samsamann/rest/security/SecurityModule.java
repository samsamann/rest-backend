package org.bitbucket.samsamann.rest.security;

import io.crnk.core.module.Module;

/**
 * security module
 */
public class SecurityModule implements Module {

    private static final String MODULE_NAME = "rest-security";

    @Override
    public String getModuleName() {
        return MODULE_NAME;
    }

    @Override
    public void setupModule(ModuleContext moduleContext) {

    }
}
