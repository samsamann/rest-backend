package org.bitbucket.samsamann.rest.base.config;

/**
 * Specific properties for this module
 */
public interface RestBackendProperties {

    String ROOT_PROP_NAME = "samsamann.rest";

    String PAGE_LIMIT_PROP_NAME = ROOT_PROP_NAME + ".default-page-limit";

    String PACKAGE_SCAN_PROP_NAME = ROOT_PROP_NAME + ".package-scan";

    Long MAX_PAGE_LIMIT = 50L;
}
