package org.bitbucket.samsamann.rest.base;

import io.crnk.core.engine.transaction.TransactionRunner;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import java.util.concurrent.Callable;

/**
 *
 */
public class SimpleTransactionRunner implements TransactionRunner {

    private EntityManager entityManager;

    public SimpleTransactionRunner(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public <T> T doInTransaction(Callable<T> callable) {
        try {
            UserTransaction transaction =
                    (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
            try {
                transaction.begin();
                entityManager.joinTransaction();
                T result = callable.call();
                transaction.commit();
                return result;
            } catch (RuntimeException e) {
                transaction.rollback();
                throw e;
            } catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } catch (SystemException | NamingException e) {
            throw new RuntimeException(e);
        }
    }
}
